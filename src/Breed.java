import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Breed {

    // Variables
    private String name;
    private List<String> color = new ArrayList<>();
    private List<String> pattern;
    private List<String> point;
    private boolean isPoint;
    private boolean withWhite;
    private int coatLength;
    private List<String> eyeColor;
    private BigDecimal low;
    private BigDecimal high;

    // Constructor
    public Breed(String name, List<String> color, List<String> pattern, List<String> point, boolean isPoint, boolean withWhite, int coatLength, List<String> eyeColor, BigDecimal low, BigDecimal high) {
        this.name = name;
        this.color = color;
        this.pattern = pattern;
        this.point = point;
        this.isPoint = isPoint;
        this.withWhite = withWhite;
        this.coatLength = coatLength;
        this.eyeColor = eyeColor;
        this.low = low;
        this.high = high;
    }

    public String toString() {
        return this.name;
    }

    // Setters + Getters
    public void setName(String val) {
        name = val;
    }

    public String getName() {
        return name;
    }

    public void setColor(List<String> val) {
        color = val;
    }

    public List<String> getColor() {
        return color;
    }

    public void setPattern(List<String> val) {
        pattern = val;
    }

    public List<String> getPattern() {
        return pattern;
    }

    public void setPoint(List<String> val) {
        point = val;
    }

    public List<String> getPoint() {
        return point;
    }

    public void setEyeColor(List<String> val) {
        eyeColor = val;
    }

    public List<String> getEyeColor() {
        return eyeColor;
    }

    public void setLow(BigDecimal val) {
        low = val;
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setHigh(BigDecimal val) {
        high = val;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public double getLowDouble() {
        return low.doubleValue();
    }

    public double getHighDouble() {
        return high.doubleValue();
    }

    // Generate random number using a given bound
    static int randomInt(int bound) {
        Random rand = new Random();
        return rand.nextInt(bound);
    }

    // Generate Sex
    private boolean generateSex() {
        Random n = new Random();
        return n.nextBoolean();
    }

    // Generate Random Cat Color
    private String generateColor() {
        return color.get(randomInt(color.size()));
    }

    // Generate Random Pattern
    private String generatePattern() {
        return pattern.get(randomInt(pattern.size()));
    }

    // Generate Whether Cat is Pointed
    private Boolean generateIsPoint(String newPoint) {
        return newPoint != "no";
    }

    // Generate Random Pattern
    private String generatePoint() {
        return point.get(randomInt(point.size()));
    }

    // Generate Random Eye Color
    private String generateEyeColor() {
        return eyeColor.get(randomInt(eyeColor.size()));
    }

    // Generate Unique Physical Traits
    private List<String> generateSpecial() {
        return Collections.emptyList();
    }

    // Generate Random Price
    private BigDecimal randomPrice() {
        double random = new Random().nextDouble();
        double price = getLowDouble() + (random * (getHighDouble() - getLowDouble()));
        return new BigDecimal(price);
    }

    // Generate Random Cat Merchandise
    public CatMerchandise randomMerchandise() {
        String tempPoint = generatePoint();
        return new CatMerchandise(generateSex(), generateColor(), generatePattern(), withWhite, generateIsPoint(tempPoint), tempPoint, coatLength, generateEyeColor(), name, generateSpecial(), randomPrice());
    }

}
