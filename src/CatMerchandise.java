// Merchandise is any Animal without an Owner, usually sold by Merchants

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

public class CatMerchandise implements Merchandise<Cat> {

    // Variables
    private boolean sex;
    private String color;
    private String pattern;
    private boolean withWhite;
    private boolean isPoint;
    private String point;
    private int coatLength; // 0 - hairless; 1 - short-hair; 2 - long-hair; 3 - intermediate
    private String eyeColor;
    private String breed;
    private List<String> special = new ArrayList<>();
    private BigDecimal price;

    // Constructor: Make nameless cats to sell
    public CatMerchandise(boolean sex, String color, String pattern, boolean withWhite, boolean isPoint, String point, int coatLength, String eyeColor, String breed, List<String> special, BigDecimal price) {
        this.sex = sex;
        this.color = color;
        this.pattern = pattern;
        this.withWhite = withWhite;
        this.isPoint = isPoint;
        this.point = point;
        this.coatLength = coatLength;
        this.eyeColor = eyeColor;
        this.breed = breed;
        this.special = special;
        this.price = price;
    }

    // To String
    public String toString() {
        return printDescription() + " (" + formatMoney(this.price) + ")";
    }

    // Setters + Getters
    public boolean getSex() {
        return sex;
    }
    public String getColor() {
        return color;
    }
    public String getPattern() {
        return pattern;
    }
    public boolean getWithWhite() {
        return withWhite;
    }
    public boolean getIsPoint() {
        return isPoint;
    }
    public String getPoint() {
        return point;
    }
    public int getCoatLength() {
        return coatLength;
    }
    public String getEyeColor() {
        return eyeColor;
    }
    public String getBreed() {
        return breed;
    }
    public List<String> getSpecial() {
        return special;
    }
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    // Short description
    public String printDescription() {
        String sexString;
        if (sex) {
            sexString = "female";
        } else {
            sexString = "male";
        }
        String description = sexString + " ";
        if (!this.color.equals("false")) {
            description += this.color + " ";
        }
        description += this.breed;
        // female gray Maine Coon
        return description;
    }

    // Exhaustive physical description
    public String describe() {
        // She is a...
        String description = capitalize(subjectivePronoun()) + " is a ";
        // seal pointed...
        if (this.isPoint && !this.point.equals("false")) {
            description += this.point + " ";
        }
        // gray
        if (!this.color.equals("false")) {
            description += this.color + " ";
        }
        // mackerel tabby...
        if (!this.pattern.equals("solid") && !this.pattern.equals("false")) {
            description += this.pattern + " ";
        }
        // long-hair...
        switch(coatLength) {
            case 0:
                description += "hairless ";
                break;
            case 1:
                description += "short-haired ";
                break;
            case 2:
                description += "long-haired ";
                break;
            case 3:
                description += "intermediate-haired ";
                break;
        }
        // Maine Coon
        if (!(this.breed.equals("mixed cat"))) {
            description +=  this.breed;
        }
        // with white...
        if (this.withWhite) {
            description += " with white";
        }
        // Her eyes are green...
        description += ". " + capitalize(possessivePronoun()) + " eyes are " + this.eyeColor + ".";
        // She has odd eyes, curly fur, patchy roan coat, and short legs.
        if (this.special.size() > 0) {
            description += " " + capitalize(subjectivePronoun()) + " has ";
            for (int i = 0; i < this.special.size(); i++) {
                description += this.special.get(i);
                if (this.special.size() > 2 && i < this.special.size() - 1) {
                    description += ", ";
                }
                if (this.special.size() == 2 && i == 0) {
                    description += "and ";
                }
                if (i == this.special.size() - 1) {
                    description += "and ";
                }
                description += ".";
            }
        }
        return description;
    }

    // Capitalize given string
    public String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    // She, He
    public String subjectivePronoun() {
        if (sex) {
            return "she";
        } else {
            return "he";
        }
    }

    // Her, Him
    public String objectivePronoun() {
        if (sex) {
            return "her";
        } else {
            return "him";
        }
    }

    // Her, His
    public String possessivePronoun() {
        if (sex) {
            return "her";
        } else {
            return "his";
        }
    }

    // Create random cat
    @Override
    public Cat createItem() {
        return new Cat("Random Cat", sex, color, isPoint, point, withWhite, pattern, coatLength, eyeColor, breed, special);
    }

    // Write Money in Currency Format
    public String formatMoney(BigDecimal val) {
        final DecimalFormat template = new DecimalFormat("#,##0.00");
        return "$" + template.format(val);
    }
}