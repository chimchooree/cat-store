import java.math.BigDecimal;
import java.util.*;

/*
The in-between area. Receive updates and choose next location.
*/

public class Plaza {

    // Variables
    public PlayerData playerData;
    private static BigDecimal STARTER_MONEY = new BigDecimal(2500);
    private static NPC mysteryCat = new NPC("Tuxedo Cat");

    // Inventory Constructor
    public Plaza (PlayerData player) {
        playerData = player;
    }

    private static Scanner ears = new Scanner(System.in); //user input
    Location catStore = new CatStore();
    Location spa = new Spa();
    Location park = new Park();
    Location home = new Home();

    Location getCatStore() {
        return catStore;
    }
    Location getSpa() {
        return spa;
    }
    Location getPark() {
        return park;
    }
    Location getHome() {
        return home;
    }

    // Is player input within bounds?
    static boolean checkDesire(int desire, int ceiling, int floor) {
        return desire <= ceiling && desire >= floor;
    }

    // Convert player input to int or return -1
    public static int chooseInt() {
        try {
            return Integer.parseInt(ears.next());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    // Generate random number using a given bound
    static int randomInt(int bound) {
        Random rand = new Random();
        return rand.nextInt(bound);
    }

    // Choose where to go next
    private boolean enter(PlayerData player) {
        int desire;
        do {
            System.out.println("Where will you go? Cat Store @ 1, Spa @ 2, Park @ 3. Home @ 4. Or 5 to quit.");
            desire = chooseInt();
        } while (!checkDesire(desire, 4, 1) && desire != 5);
        switch (desire) {
            case 1:
                getCatStore().enter(this, player);
                return true;
            case 2:
                getSpa().enter(this, player);
                return true;
            case 3:
                getPark().enter(this, player);
                return true;
            case 4:
                getHome().enter(this, player);
                return true;
            case 5:
                return false;
        }
        return false;
    }

    // Choose and Confirm Player Name
    private static String chooseName() {
        // Choose Name
        String name = ears.nextLine();
        // Confirm Name
        if (name.equals(mysteryCat.getName())) {
            System.out.println(mysteryCat + ": Nice name.");
        }
        System.out.println(mysteryCat + ": Is " + name + " what you'd like to go by?");
        String response = ears.nextLine();
        response = response.toLowerCase();
        if (!response.startsWith("y")) {
            System.out.println(response); //response is set in stone after first
            System.out.println(mysteryCat + ": Is that a no? What would you like to go by?");
            return chooseName();
        }
        return name;
    }

    // Ask for Player Name
    private static void welcome() {
        System.out.println(mysteryCat + ": Welcome.");
        System.out.println(mysteryCat + ": May I have your name?");
    }

    // Give Player Starter Money
    private static void endIntro(PlayerData player) {
        System.out.println(mysteryCat + ": Before I leave, I have a gift for you. Spend it wisely.");
        System.out.println(player.getName() + " receives $"+ STARTER_MONEY + ".00. ");
        player.setMoney(player.getMoney().add(STARTER_MONEY));
        System.out.println(mysteryCat + ": Until we meet again...");
        System.out.println("The cat is gone with the blink of an eye.");
    }

    // Start of Program
    public static void main(String[] args) {
        // Intro
        welcome();
        String name = chooseName();
        // Setup
        PlayerData player = new PlayerData(name,new BigDecimal(0));
        Plaza plaza = new Plaza(player);
        endIntro(player);
        // Game Loop
        while(plaza.enter(player));
    }
}

