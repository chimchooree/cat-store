import java.math.BigDecimal;

public interface Merchandise<T> {
    BigDecimal getPrice();
    T createItem();
}
