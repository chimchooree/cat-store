import java.math.BigDecimal;
import java.text.DecimalFormat;

public class ItemMerchandise implements Merchandise<Item> {

    // Merchandise is any item for sale by Merchants

    // Variables
    private String name;
    private BigDecimal basePrice;

    // Make nameless cats to sell
    public ItemMerchandise(String name, BigDecimal basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    // To String
    public String toString() {
        return this.name + " (" + formatMoney(this.basePrice) + ")";
    }

    // Getters + Setters
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getPrice() {
        return basePrice;
    }

    // Put Money in Currency Format
    private String formatMoney(BigDecimal val) {
        final DecimalFormat template = new DecimalFormat("0.00");
        return "$" + template.format(val);
    }

    // Create Item
    @Override
    public Item createItem() {
        return new Item("Item", basePrice);
    }
}
