import java.math.BigDecimal;
import java.util.*;

/*
Location: Spa
Maintain your cat's hygiene and buy grooming items.
*/

public class Spa implements Location {

    // Variables
    private NPC spaMerch = new NPC("Delphi");
    private List<ItemMerchandise> spaMerchandise = new ArrayList<>();

    // Inventory Constructor
    public Spa () {
        spaMerchandise.add(new ItemMerchandise("Flea Comb", new BigDecimal(3.97)));
        spaMerchandise.add(new ItemMerchandise("Nail Clippers", new BigDecimal(5.94)));
        spaMerchandise.add(new ItemMerchandise("Shampoo", new BigDecimal(6.99)));
    }

    // Check given Item's Price against Player's funds
    private boolean canAfford(int i, PlayerData player) {
        return player.getMoney().compareTo(spaMerchandise.get(i).getPrice()) >= 0;
    }

    // Purchase given Item
    private void checkout(int i, PlayerData player) {
        if (canAfford(i, player)) {
            Item item = new Item(spaMerchandise.get(i).getName(), spaMerchandise.get(i).getPrice());
            if (player.addItem(item)) {
                System.out.println(spaMerchandise.get(i).getName() + " purchased.");
                player.setMoney(player.getMoney().subtract(spaMerchandise.get(i).getPrice()));
            } else {
                System.out.println("Inventory full.");
            }
        } else {
            System.out.println(spaMerch + ": Your card declined...Do you have another?");
        }
    }

    // Entry Point from Plaza
    @Override
    public void enter(Plaza plaza, PlayerData player) {
        System.out.println(spaMerch + ": Hello, welcome to the Castalian Spa. ");
        System.out.println(spaMerch + ": What can I do for you?");
        spa(spaMerch, player);
    }

    // Display Spa Menu
    private void spa(NPC spaMerch, PlayerData player) {
        int desire;
        do {
            System.out.println(spaMerch + ": Press 1 for a spa treatment, 2 to look at my wares, and 3 to leave.");
            desire = Plaza.chooseInt();
        } while (!Plaza.checkDesire(desire, 3, 1));
        switch (desire) {
            case 1:
                spaTreatment(spaMerch, player);
                spa(spaMerch, player);
                break;
            case 2:
                spaShop(spaMerch, player);
                spa(spaMerch, player);
                break;
            case 3:
                break;
        }
    }

    // Improve Cat's Hygiene
    private void spaTreatment(NPC spaMerch, PlayerData player) {
        if (player.countCats() >= 1) {
            Cat cat = player.chooseCat(0);
            if (player.countCats() > 1) {
                cat = player.chooseCat(player);
            }
            System.out.println(spaMerch + " sits on top of " + cat + " and licks " + cat.objectivePronoun() + " down.");
        } else {
            System.out.println(spaMerch + ": Our spa service is only for cats. Sorry for the inconvenience.");
        }
    }

    // Buy Hygiene Items to Improve Cat's Hygiene at Home
    private void spaShop(NPC spaMerch, PlayerData player) {
        System.out.println(spaMerch + "'s Wares: " + spaMerchandise.get(0) + ", " + spaMerchandise.get(1) + ", and " + spaMerchandise.get(2) + ".");
        int desire;
        do {
            System.out.println("Press 1-3 to purchase. Or press 4 to stop browsing.");
            desire = Plaza.chooseInt();
        } while (!Plaza.checkDesire(desire, 4, 1));
        switch (desire) {
            case 1:
                checkout(0, player);
                spaShop(spaMerch, player);
            case 2:
                checkout(1, player);
                spaShop(spaMerch, player);
            case 3:
                checkout(2, player);
                spaShop(spaMerch, player);
            case 4:
                break;
        }
    }
}
