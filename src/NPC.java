public class NPC {

    // Variables
    private String name;

    // Constructor
    public NPC(String name) {
            this.name = name;
        }

    // To String
    public String toString() {
            return name;
        }

    // Setters + Getters
    public String getName() { return name; }
}
