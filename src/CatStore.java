import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/*
Location: Cat Store
Buy cats at the Cat Store
*/

public class CatStore implements Location {

    // Variables
    private static final Scanner ears = new Scanner(System.in); //user input
    private NPC catMerch = new NPC("Black Cat");
    private List<CatMerchandise> catMerchandise = new ArrayList<>();
    private List<Breed> catBreeds = new ArrayList<>();
    private int inventorySize = 3;

    // Inventory Constructor
    public CatStore () {
        // Read the breed.box file
        try {
            BufferedReader catFile = new BufferedReader(new FileReader("breed.box"));
            String line = catFile.readLine();
            while(line != null) {

                String name = line;
                List<String> colorLine = Arrays.asList(catFile.readLine().split(",")); // can be false
                List<String> patternLine = Arrays.asList(catFile.readLine().split(",")); // can be false
                String pointTemp = catFile.readLine();
                List<String> pointLine;
                boolean isPointLine;
                if (pointTemp == "false") {
                    pointLine = Collections.emptyList();
                    isPointLine = false;
                } else {
                    pointLine = Arrays.asList(pointTemp.split(","));
                    isPointLine = true;
                }
                boolean whiteLine = catFile.readLine() == "true";
                int coatLengthLine = Integer.parseInt(catFile.readLine());
                List<String> eyeLine = Arrays.asList(catFile.readLine().split(","));
                String[] priceLine = catFile.readLine().split(",");
                double minPrice = Double.parseDouble(priceLine[0]);
                double maxPrice = Double.parseDouble(priceLine[1]);

                Breed breed = new Breed (

                        name,
                        colorLine,
                        patternLine,
                        pointLine,
                        isPointLine,
                        whiteLine,
                        coatLengthLine,
                        eyeLine,
                        new BigDecimal(minPrice),
                        new BigDecimal(maxPrice)
                );
                catBreeds.add(breed);

                line = catFile.readLine();

            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        for (int i = 0; i < inventorySize; i++) {
            catMerchandise.add(generateRandomCat());
        }
    }

    // Get a Random Breed
    private Breed chooseRandomBreed() {
        return catBreeds.get(new Random().nextInt(catBreeds.size()));
    }

    // Make Merchandise in a Random Breed
    private CatMerchandise generateRandomCat() {
        return chooseRandomBreed().randomMerchandise();
    }

    // Choose + Confirm Cat Name
    private String nameCat(CatMerchandise catMerchandise) {
        String name = ears.nextLine();
        System.out.println("Would you like to name your cat " + name + "?");
        String response = ears.nextLine();
        response = response.toLowerCase();
        if (!response.startsWith("y")) {
            System.out.println(response);
            System.out.println("Think about it. What would you like to name " + catMerchandise.objectivePronoun() + "?");
            return nameCat(catMerchandise);
        }
        return name;
    }

    // Convert CatMerchandise to Cat with given Name
    private Cat adoptCat(CatMerchandise catMerchandise) {
        System.out.println("What would you like to name your cat?");
        return new Cat(nameCat(catMerchandise), catMerchandise.getSex(), catMerchandise.getColor(), catMerchandise.getIsPoint(), catMerchandise.getPoint(), catMerchandise.getWithWhite(),catMerchandise.getPattern(),catMerchandise.getCoatLength(),catMerchandise.getEyeColor(),catMerchandise.getBreed(),catMerchandise.getSpecial());
    }

    // Add CatMerchandise to Stock
    private void addProduct(CatMerchandise cat) {
        catMerchandise.add(cat);
    }

    // Remove given Item from Stock
    private void removeProduct(int i) {
        catMerchandise.remove(i);
    }

    // Check given Item Price against Player funds
    private boolean canAfford(int i, PlayerData player) {
        return player.getMoney().compareTo(catMerchandise.get(i).getPrice()) >= 0;
    }

    // Need to remove cat from store, replenish stock with new cat
    private void checkout(int i, PlayerData player) {
        if (canAfford(i, player)) {
            Cat cat = adoptCat(catMerchandise.get(i));
            if (player.adoptCat(cat)) {
                System.out.println(player.getName() + " adopted " + cat + ".");
                player.setMoney(player.getMoney().subtract(catMerchandise.get(i).getPrice()));
                removeProduct(i);
                System.out.println(catMerch + ": finally. good riddance stupid " + cat.getBreed() + "!!");
                addProduct(generateRandomCat());
            } else {
                System.out.println("That's one cat too many to adopt. Increase your cat capacity or get rid of an old cat.");
            }
        } else {
            System.out.println(catMerch + ": no cash, no cat.");
        }
    }

    // See Cat's Description before Adopting or Browsing
    private void examineCat(int i, PlayerData player) {
        System.out.println(catMerchandise.get(i).describe());
        System.out.println(catMerch + ": so u gonna buy? " + catMerchandise.get(i).formatMoney(catMerchandise.get(i).getPrice()) + ".");
        int desire;
        do {
            System.out.println("Press 1 to adopt or 2 to browse.");
            desire = Plaza.chooseInt();
        } while (!Plaza.checkDesire(desire,2,1));
        switch(desire) {
            case 1:
                checkout(i,player);
                break;
            case 2:
                break;
        }

    }

    // Display Cat Shop menu
    private void catShop(PlayerData player) {
        int desire;
        do {
            System.out.println(catMerch + ": press 1 to see " + catMerchandise.get(0) + ", 2 to see " + catMerchandise.get(1) + ", or 3 to see " + catMerchandise.get(2) + ". or u can press 4 to leave.");
            desire = Plaza.chooseInt();
        } while (!Plaza.checkDesire(desire, 4, 1));
        switch(desire) {
            case 1:
                examineCat(0, player);
                break;
            case 2:
                examineCat(1, player);
                break;
            case 3:
                examineCat(2, player);
                break;
            case 4:
                System.out.println(catMerch + ": smell u later");
                return;
        }
        catShop(player);
    }

    // Entry Point
    @Override
    public void enter(Plaza plaza, PlayerData player) {
        System.out.println(catMerch + ": hey you. welcome to cat store !!");
        catShop(player);
    }
}