import java.math.BigDecimal;

public class Item {

    // Variables
    private String name;
    private BigDecimal basePrice;

    // Constructor
    public Item(String name, BigDecimal basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    // To String
    public String toString() {
        return this.name + " ($" + this.basePrice + ")";
    }

    // Getters + Setters
    public String getName() {
        return name;
    }


}