/*
Location: Park
Have fun with your cat and experience random events.
*/

import java.math.BigDecimal;
import java.util.Scanner;

public class Park implements Location {

    // Variables
    private static final Scanner ears = new Scanner(System.in); //user input

    // If Player has no Cats
    private void noCat() {
        System.out.println("You don't have a cat, so you probably wouldn't fit in.");
        System.out.println("You should leave for now...");
    }

    // Park Event
    private void park(PlayerData player, Cat cat) {
        System.out.println("You arrive at the park with " + cat + ".");
        String capitalCat = cat.getName().substring(0,1).toUpperCase() + cat.getName().substring(1);
        switch (Plaza.randomInt(3)) {
            case 0:
                System.out.println(capitalCat + " finds $50 under a park bench. Good kitty!");
                player.setMoney(player.getMoney().add(new BigDecimal(50)));
                break;
            case 1:
                System.out.println(capitalCat + " is confronted by " + cat.possessivePronoun() + " rival. This could turn ugly...");
                break;
            case 2:
                System.out.println(capitalCat + " stiffens into a statue. This walk is going nowhere.");
                break;
        }
    }

    // Entry Point
    @Override
    public void enter(Plaza plaza, PlayerData player) {
        if (player.countCats() != 0) {
            Cat cat = player.chooseCat(0);
            if (player.countCats() > 1) {
                cat = player.chooseCat(player);
            }
            park(player, cat);
        } else {
            noCat();
        }
    }
}