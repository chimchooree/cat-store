import java.util.*;

public class Cat {

    // Variables
    private Scanner ears = new Scanner(System.in);
    private boolean sex;
    private String color;
    private boolean isPoint;
    private String point;
    private boolean withWhite;
    private String pattern;
    private int coatLength;
    private String eyeColor;
    private String breed;
    private List<String> special = new ArrayList<>();
    private String name;
    private List<String> genes = new ArrayList<>();
    private int age; // in days

    // Constructor
    public Cat(String name, boolean sex, String color, boolean isPoint, String point, boolean withWhite, String pattern, int coatLength, String eyeColor, String breed, List<String> special) {
        this.name = name;
        this.sex = sex;
        this.color = color;
        this.isPoint = isPoint;
        this.point = point;
        this.withWhite = withWhite;
        this.pattern = pattern;
        this.coatLength = coatLength;
        this.eyeColor = eyeColor;
        this.breed = breed;
        this.special = special;
    }

    // To String
    public String toString() {
        return this.name;
    }

    // Setters + Getters
    public void setName(String val) {
        name = val;
    }
    public String getName() {
        return name;
    }
    public void setSex(boolean val) {
        sex = val;
    }
    public boolean getSex() {
        return sex;
    }
    public void setColor(String val) {
        color = val;
    }
    public String getColor() {
        return color;
    }
    public boolean getIsPoint() { return isPoint; }
    public String getPoint() { return point; }
    public boolean getWithWhite() { return withWhite; }
    public String getPattern() { return pattern; }
    public int getCoatLength() { return coatLength; }
    public String getEyeColor() { return eyeColor; }
    public void setBreed(String val) {
        breed = val;
    }
    public String getBreed() {
        return breed;
    }
    public List<String> getSpecial() { return special; }

    // Enter + Confirm Name
    public String chooseName() {
        // Choose Name
        String name = ears.nextLine();
        // Confirm Name
        System.out.println("Is " + name + " what you'd like to call " + objectivePronoun() + "? ");
        String response = ears.nextLine();
        response = response.toLowerCase();
        if (!response.startsWith("y")) {
            System.out.println(response);
            System.out.println("Try again: ");
            return chooseName();
        }
        return name;
    }

    // Short description
    public String printDescription() {
        String sexString;
        if (sex) {
            sexString = "female";
        } else {
            sexString = "male";
        }
        String description = sexString + " ";
        if (!this.color.equals("false")) {
            description += this.color + " ";
        }
        description += this.breed;
        // female gray Maine Coon
        return description;
    }

    // Exhaustive physical description
    public String describe() {
        // She is a...
        String description = this.name + " is a ";
        // seal pointed...
        if (this.isPoint && !this.point.equals("false")) {
            description += this.point + " ";
        }
        // gray
        if (!this.color.equals("false")) {
            description += this.color + " ";
        }
        // mackerel tabby...
        if (!this.pattern.equals("solid") && !this.pattern.equals("false")) {
            description += this.pattern + " ";
        }
        // long-hair...
        switch(coatLength) {
            case 0:
                description += "hairless ";
                break;
            case 1:
                description += "short-haired ";
                break;
            case 2:
                description += "long-haired ";
                break;
            case 3:
                description += "intermediate-haired ";
                break;
        }
        // Maine Coon
        description +=  this.breed;
        // with white...
        if (this.withWhite) {
            description += " with white";
        }
        // Her eyes are green...
        description += ". " + capitalize(possessivePronoun()) + " eyes are " + this.eyeColor + ".";
        // She has odd eyes, curly fur, patchy roan coat, and short legs.
        if (this.special.size() > 0) {
            description += " " + capitalize(subjectivePronoun()) + " has ";
            for (int i = 0; i < this.special.size(); i++) {
                description += this.special.get(i);
                if (this.special.size() > 2 && i < this.special.size() - 1) {
                    description += ", ";
                }
                if (this.special.size() == 2 && i == 0) {
                    description += "and ";
                }
                if (i == this.special.size() - 1) {
                    description += "and ";
                }
                description += ".";
            }
        }
        return description;
    }

    // Capitalize given string
    public String capitalize(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    // She or He
    public String subjectivePronoun() {
        if (sex) {
            return "she";
        } else {
            return "he";
        }
    }

    // Her or Him
    public String objectivePronoun() {
        if (sex) {
            return "her";
        } else {
            return "him";
        }
    }

    // Her or His
    public String possessivePronoun() {
        if (sex) {
            return "her";
        } else {
            return "his";
        }
    }

}
