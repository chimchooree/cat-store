import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

public class PlayerData {

    // Variables
    private String name;
    private BigDecimal money;
    private List<Cat> catSlots = new ArrayList<>();
    private int catLimit = 3; // cat ownership maximum
    private List<Item> inventory = new ArrayList<>();
    private static final Scanner ears = new Scanner(System.in); //user input

    // Constructor
    public PlayerData(String name, BigDecimal money) {
        this.name = name;
        this.money = money;
    }

    // To String
    public String toString() {
        return this.name;
    }

    // Setters + Getters
    public void setName(String val) {
        name = val;
    }

    public String getName() {
        return name;
    }

    public void setMoney(BigDecimal val) {
        money = val;
        String formatted = formatMoney(val);
        System.out.println(name + " now has " + formatted + ".");
    }

    public BigDecimal getMoney() {
        return money;
    }

    // Get Player Money as a String in Currency Format
    public String getMoneyAsString() {
        return formatMoney(money);
    }

    // Ready Money for Printing in Currency Format
    private String formatMoney(BigDecimal val) {
        final DecimalFormat template = new DecimalFormat("0.00");
        return "$" + template.format(val);
    }

    // Add a Cat to catSlots
    public boolean adoptCat(Cat cat) {
        if (catSlots.size() >= catLimit) {
            return false;
        }
        return catSlots.add(cat);
    }

    // Add an Item to Inventory
    public boolean addItem(Item item) {
        if (inventory.size() >= 20) {
            return false;
        }
        return inventory.add(item);
    }

    // Return Number of Cats Owned by Player
    public int countItems() {
        return inventory.size();
    }

    public List<Item> displayInventory() {
        return inventory;
    }

    // Return Number of Cats Owned by Player
    public int countCats() {
        return catSlots.size();
    }

    public List<Cat> displayCats() {
        return catSlots;
    }

    // Return given Cat
    public Cat chooseCat(int i) {
        return catSlots.get(i);
    }

    public Cat randomCat() {
        return catSlots.get(new Random().nextInt(catSlots.size()));
    }

    private boolean checkCat(PlayerData player, int i) {
        if (i < player.countCats() && i >= 0) {
            return true;
        } else {
            return false;
        }
    }

    // Convert player input to int or return -1
    public static int chooseInt() {
        try {
            return Integer.parseInt(ears.next());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    // Print press 0 for cat 1, 1 for cat 2, etc.
    public void listCats(PlayerData player) {
        System.out.print("Press ");
        for (int i = 0; i < player.countCats(); i++) {
            System.out.print(i + " for " + player.chooseCat(i).getName());
            if (i < player.countCats() - i && player.countCats() - i != 2) {
                System.out.print(", ");
            } if (player.countCats() - i == 2) {
                System.out.print(", or ");
            } if (player.countCats() - i == 1) {
                System.out.print(": ");
            }
        }
    }

    // Choose Cat - arg not needed?
    public Cat chooseCat(PlayerData player) {
        int choice;
        System.out.println("Choose a cat: ");
        listCats(player);
        choice = chooseInt();
        if (!checkCat(player, choice)) {
            return chooseCat(player);
        }
        return player.chooseCat(choice);
    }

    public List<Cat> findEligibleMoms() {
        List<Cat> moms = new ArrayList<>();
        for (int i = 0; i < catSlots.size(); i++) {
            if (catSlots.get(i).getSex()) {
                moms.add(catSlots.get(i));
            }
        }
        return moms;
    }

    public List<Cat> findEligibleDads() {
        List<Cat> dads = new ArrayList<>();
        for (int i = 0; i < catSlots.size(); i++) {
            if (!catSlots.get(i).getSex()) {
                dads.add(catSlots.get(i));
            }
        }
        return dads;
    }

    public Cat chooseParent(List<Cat> cats, boolean b) {
        int choice;

        System.out.print("Choose a ");

        // Correct sex
        if (b) {
            System.out.println("dam: ");
        } else {
            System.out.println("sire: ");
        }

        // Display eligible parents
        System.out.print("Press ");
        for (int i = 0; i < cats.size(); i++) {
            System.out.print(i + " for " + cats.get(i).getName());
            if (cats.size() - i == 2) {
                System.out.print(", or ");
            } if (cats.size() - i == 1) {
                System.out.print(": ");
            } else {
                System.out.print(", ");
            }
        }

        // Choose Parent
        choice = chooseInt();
        // Is player input within bounds?
        while (!(choice < cats.size() && choice >= 0)) {
            System.out.println("Number out of bounds. Try again: ");
            choice = chooseInt();
        }

        // Return Parent
        return cats.get(choice);
    }

    public int canBreed() {
        if (catSlots.size() >= catLimit) {
            // Too many cats
            return 0;
        } if (countCats() < 2) {
            // Not enough cats
            return 1;
        } if (findEligibleMoms().size() < 1 && findEligibleDads().size() < 1) {
            // Not enough eligible cats
            return 2;
        } else {
            // Can breed!
            return 3;
        }
    }

    // Breed Cats
    public Cat breedCats() {
        Cat mom = chooseParent(findEligibleMoms(), true);
        Cat dad = chooseParent(findEligibleDads(), false);
        Cat baby = new Cat ("Kitten", true, mom.getColor(), mom.getIsPoint(), mom.getPoint(), mom.getWithWhite(), mom.getPattern(), mom.getCoatLength(), mom.getEyeColor(), "mixed cat", mom.getSpecial());
        adoptCat(baby);
        return baby;
    }
}
