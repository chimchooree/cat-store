/*
Location: Home
Bond with your cats, manage your resources, and experience random events.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class Home implements Location {

    // Variables
    private static Scanner ears = new Scanner(System.in); //user input
    private NPC mysteryCat = new NPC("Tuxedo Cat");
    private List<Breed> catBreeds = new ArrayList<>();
    private boolean firstVisit = true;

    // Fill catBreeds with Breeds from breed.box
    private void readCats() {
        // Read the breed.box file
        try {
            BufferedReader catFile = new BufferedReader(new FileReader("breed.box"));
            String line = catFile.readLine();
            while(line != null) {
                // Convert file lines to Breed data
                String name = line;
                List<String> colorLine = Arrays.asList(catFile.readLine().split(",")); // can be false
                List<String> patternLine = Arrays.asList(catFile.readLine().split(",")); // can be false
                String pointTemp = catFile.readLine();
                List<String> pointLine;
                boolean isPointLine;
                if (pointTemp == "false") {
                    pointLine = Collections.emptyList();
                    isPointLine = false;
                } else {
                    pointLine = Arrays.asList(pointTemp.split(","));
                    isPointLine = true;
                }
                boolean whiteLine = catFile.readLine() == "true";
                int coatLengthLine = Integer.parseInt(catFile.readLine());
                List<String> eyeLine = Arrays.asList(catFile.readLine().split(","));
                String[] priceLine = catFile.readLine().split(",");
                double minPrice = Double.parseDouble(priceLine[0]);
                double maxPrice = Double.parseDouble(priceLine[1]);

                // Form Breed from data
                Breed breed = new Breed (
                        name,
                        colorLine,
                        patternLine,
                        pointLine,
                        isPointLine,
                        whiteLine,
                        coatLengthLine,
                        eyeLine,
                        new BigDecimal(minPrice),
                        new BigDecimal(maxPrice)
                );

                // Add Breed to catBreeds
                catBreeds.add(breed);

                line = catFile.readLine();

            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    // Get a random Breed
    private Breed chooseRandomBreed() {
        readCats();
        return catBreeds.get(new Random().nextInt(catBreeds.size()));
    }

    // Choose + Confirm Name with Mysterious Cat
    private String chooseName(String sexString) {
        // Choose Name
        String name = ears.nextLine();
        // Confirm Name
        System.out.println(mysteryCat + ": Is " + name + " what you'd like to call " + sexString + "?");
        String response = ears.nextLine();
        response = response.toLowerCase();
        if (!response.startsWith("y")) {
            System.out.println(response);
            System.out.println(mysteryCat + ": Is that a no? One more time-?");
            return chooseName(sexString);
        }
        return name;
    }

    // Breed Cats + Name Kitten
    private void breedCats(PlayerData player) {
        Cat cat = player.breedCats();
        System.out.println(cat + " is born!");
        System.out.println("Choose a name for " + cat.objectivePronoun() + ": ");
        cat.setName(cat.chooseName());
    }

    // Check if able to breed
    private void canBreed(PlayerData player) {
        switch (player.canBreed()) {
            case 0:
                System.out.println("There's not enough room for another cat.");
                break;
            case 1:
                System.out.println("I don't even have two cats.");
                break;
            case 2:
                System.out.println("I don't have two eligible cats.");
                break;
            case 3:
                breedCats(player);
                break;
        }
    }

    // Display Player's Inventory
    private void checkInventory(PlayerData player) {
        System.out.println("You go through your cabinets and pantry. You have...");
        if (player.countItems() <= 0) {
            System.out.println("nothing...");
        }
        for (int i = 0; i < player.countItems(); i++) {
            System.out.println(player.displayInventory().get(i).getName());
        }
    }

    // Examine the appearance + condition of Player's chosen Cat
    private void checkCats(PlayerData player) {
        // Multiple Cats Owned
        if (player.countCats() > 1) {
            System.out.println("You call for your cats.");
            Cat cat = player.chooseCat(player);
            System.out.println(cat.describe());
            // also print wellness, age
        }
        // One Cat Owned
        if (player.countCats() == 1) {
            System.out.println("You call " + player.displayCats().get(0) + " over for a look.");
            for (int i = 0; i < player.countCats(); i++) {
                System.out.println(player.displayCats().get(i).describe());
                // also print wellness, age
            }
        // No Cats Owned
        } if (player.countCats() <= 0) {
            System.out.println("You don't have any cats.");
        }
    }

    // Home Loop
    private void home(PlayerData player) {
        int desire;
        do {
            System.out.println("Press 1 to see your cats, 2 to check your inventory, 3 to check your wallet, 4 to breed cats, or 5 to leave.");
            desire = Plaza.chooseInt();
        } while (!Plaza.checkDesire(desire, 5, 1));
        switch(desire) {
            case 1:
                checkCats(player);
                break;
            case 2:
                checkInventory(player);
                break;
            case 3:
                System.out.println("You open your banking app and see " + player.getMoneyAsString() + ".");
                break;
            case 4:
                canBreed(player);
                break;
            case 5:
                return;
        }
        home(player);
    }

    // for firstNoCat()
    private String aOrAn(CatMerchandise cat) {
        if (cat.getColor().toLowerCase().startsWith("a") | cat.getColor().startsWith("e") | cat.getColor().startsWith("i") | cat.getColor().startsWith("o") | cat.getColor().startsWith("u")) {
            return "An";
        } else {
            return "A";
        }
    }

    // First Time @ Home - Own at least 1 Cat
    private void firstCat(PlayerData player) {
        // different cats respond different - alpha, beta, etc
        System.out.println(player.randomCat() + " hides behind your leg, eyes and ears on full alert.");
        System.out.println(mysteryCat + ": I see you made a friend. That's good. ");
        System.out.println(mysteryCat + ": Cats are everything in this world.");
        System.out.println(mysteryCat + ": Since you seem to be doing well, I'll leave you. For now...");
        System.out.println("The cat disappears again.");
    }

    // First Time @ Home - Own no Cat
    private void firstNoCat(PlayerData player) {
        CatMerchandise cat = chooseRandomBreed().randomMerchandise();

        System.out.println(mysteryCat + ": Why are you always alone? Everyone has a cat but you.");
        System.out.println(mysteryCat + ": I can change that.");
        System.out.println(aOrAn(cat) + " " + cat.getColor() + " " + cat.getBreed() + " appears and sits next to the mysterious cat.");

        int desire;
        do {
            System.out.println("Take a closer look at the new cat (1) or Tuxedo Cat (2): ");
            desire = Plaza.chooseInt();
        } while (!Plaza.checkDesire(desire,2,1));
        switch(desire) {
            case 1:
                System.out.println(cat.describe());
                break;
            case 2:
                System.out.println(player + ": I'd rather adopt you.");
                System.out.println(mysteryCat + ": Me? I'm not a cat.");
                break;
        }
        System.out.println(mysteryCat + ": Go ahead. Give " + cat.objectivePronoun() + " a name: ");
        String name = chooseName(cat.objectivePronoun());
        player.adoptCat(new Cat(name, cat.getSex(), cat.getColor(), cat.getIsPoint(), cat.getPoint(), cat.getWithWhite(), cat.getPattern(), cat.getCoatLength(),cat.getEyeColor(),cat.getBreed(),cat.getSpecial()));
        System.out.println(mysteryCat + ": The cat's all yours. Enjoy.");
        System.out.println(mysteryCat + ": Make the most of " + cat.objectivePronoun() + ". I am always watching...");
        System.out.println("Without a good-bye, he vanishes, leaving " + name + " behind.");

    }

    // First Visit to Home
    private void firstVisit(PlayerData player) {
        System.out.println("Chatoyant eyes stare from the empty room.");
        System.out.println(mysteryCat + ": We meet again.");
        // If Player has a Cat
        if (player.countCats() != 0) {
            firstCat(player);
        // If Player has no Cats
        } else {
            firstNoCat(player);
        }
        firstVisit = false;
    }

    // Entry Point
    @Override
    public void enter(Plaza plaza, PlayerData player) {
        System.out.println("You arrive at your dilapidated apartment.");
        if (firstVisit) {
            firstVisit(player);
        }
        // random event possibility
        System.out.println("You have a moment to gather your thoughts.");
        home(player);
    }
}