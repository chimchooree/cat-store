public interface Location {
    void enter(Plaza plaza, PlayerData player);
}
